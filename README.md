# hacks-skill

A collection of hacks to work around problems with the Mycroft framework that
haven't been patched yet.


## Why This exists

Mycroft is awesome, but as it's still a young company with few resources, weird
bugs/problems can take a while to be hammered out upstream.  I created this
"skill" to work-around such problems until they're fixed, at which point I'll
just remove those hacks from here.


## What's in here?

Just one hack at the moment wherein settings updated on `home.mycroft.ai` don't
appear to be updating locally, no matter how many reboots I run or minutes I
wait.  The only thing that actually appears to trigger a refresh of settings is
`touch`-ing the `__init__.py` file for each skill.  So, this hack listens for
the `mycroft.skills.settings.update` event, and when it comes through, this
hack will `touch()` every `__init__.py` file in every installed skill.

There appears to be a number of attempts to solve this problem, but at present
none of the proposed solutions have been accepted into the mainline, so that's
why this exists.

Relevant reference links:
[#2707](https://github.com/MycroftAI/mycroft-core/pull/2707),
[#2698](https://github.com/MycroftAI/mycroft-core/pull/2698).
