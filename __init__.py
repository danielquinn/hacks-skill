import logging

from pathlib import Path
from mycroft import MycroftSkill

logger = logging.getLogger(__name__)


class HacksSkill(MycroftSkill):
    """
    None of these should be necessary long-term, but in the short term, while
    the developers work things out on their end, this "skill" is meant to be a
    collection of workarounds for things that haven't been fixed yet.
    """

    def initialize(self):
        self.add_event(
            "mycroft.skills.settings.update",
            self.handler_mycroft_skills_settings_update,
        )

    def handler_mycroft_skills_settings_update(self, *args):
        """
        There's a bug in how Mycroft handles settings updates that basically
        prevents any settings from actually getting updated.  The only way to
        make it work that I've found is to update the settings and then touch
        the __init__.py file on the affected skill.  This will restart things
        on Mycroft's end, meaning that the new settings will take effect.
        """

        self.log.info(
            "HACK: Settings update triggered.  "
            "Touching all skill init files to force a reload."
        )

        for f in Path(__file__).parent.parent.glob("*/__init__.py"):
            f.touch()


def create_skill():
    return HacksSkill()
